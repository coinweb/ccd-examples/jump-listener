import {
	type NewTx,
	getMethodArguments,
	constructContractIssuer,
	addMethodHandler,
	SELF_REGISTER_HANDLER_NAME,
	executeHandler,
	getContractId,
	selfCallWrapper,
	extractContractInfo,
	type Context,
	constructContractRef,
	type PreparedCallInfo,
	bigIntReplacer,
	constructJumpTx,
	type BlockFilter,
	constructBlock,
	getNextContinuation,
	getContextSystem,
	type Shard,
	getParent,
	type ClaimIssuer,
	type PreparedOperation,
	constructRead,
	constructClaimKey,
	constructRangeRead,
	extractContractArgs,
	extractRead,
	extractBlock,
	type IssuedClaim,
	constructStore,
	constructClaim,
	toHex,
	constructContinueTx,
	bigIntReviver,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";
import {
	JUMP_AND_WAIT,
	RETURN_BACK,
	SAVE_RESULTS,
} from "../offchain/constants";

const constructReadByFilter = (filter: BlockFilter): PreparedOperation => {
	if (filter.second) {
		return constructRead(
			filter.issuer,
			constructClaimKey(filter.first, filter.second),
		);
	}
	return constructRangeRead(filter.issuer, filter.first, {}, 1000);
};

const prepareReturnBackCall = (
	context: Context,
	currentShard: Shard,
	caller: ClaimIssuer,
	blockFilter: BlockFilter,
): PreparedCallInfo => {
	const self = getContractId(context.tx);
	const preparedCallInfo: PreparedCallInfo = {
		ref: constructContractRef(constructContractIssuer(self), []),
		methodInfo: {
			methodName: RETURN_BACK,
			methodArgs: [currentShard, caller],
		},
		contractInfo: {
			providedCweb: 1500n,
			authenticated: null,
		},
		contractArgs: [
			constructReadByFilter(blockFilter),
			constructBlock([blockFilter]),
		],
	};
	return preparedCallInfo;
};

const prepareSaveResultsCall = (
	context: Context,
	currentShard: Shard,
	caller: ClaimIssuer,
	filter: BlockFilter,
	results: IssuedClaim[],
): PreparedCallInfo => {
	const self = getContractId(context.tx);
	const preparedCallInfo: PreparedCallInfo = {
		ref: constructContractRef(constructContractIssuer(self), []),
		methodInfo: {
			methodName: SAVE_RESULTS,
			methodArgs: [caller, currentShard, filter, results],
		},
		contractInfo: {
			providedCweb: 200n,
			authenticated: null,
		},
		contractArgs: [],
	};
	return preparedCallInfo;
};

const jumpAndWait = (context: Context): NewTx[] => {
	const [_, shard, filter] = getMethodArguments(context);
	const currentShard = getContextSystem().shard;
	const caller = getParent(context.call);
	const returnBackCall = prepareReturnBackCall(
		context,
		currentShard,
		caller,
		filter,
	);
	return [constructJumpTx(context, shard, [], [{ callInfo: returnBackCall }])];
};

const returnBack = (context: Context): NewTx[] => {
	const [_, shard, issuer] = getMethodArguments(context);
	const [readOp, blockOp] = extractContractArgs(context.tx);
	const currentShard = getContextSystem().shard;
	const results = extractRead(readOp) ?? [];
	const blockFilter = extractBlock(blockOp);
	if (!blockFilter || blockFilter.length === 0) {
		throw new Error("Invalid block filter");
	}
	const [filter, triggered] = blockFilter[0];
	if (!triggered) {
		throw new Error("Filter not triggered");
	}
	const saveResultsCall = prepareSaveResultsCall(
		context,
		currentShard,
		issuer,
		filter,
		results,
	);
	return [constructJumpTx(context, shard, [], [{callInfo: saveResultsCall}])];
};

const saveResults = (context: Context): NewTx[] => {
	const [_, caller, shard, filter, results] = getMethodArguments(context);
	return [
		constructContinueTx(context, [
			constructStore(constructClaim(constructClaimKey(caller, [shard, filter]), results, toHex(0n)))
		])
	];
};

export function cwebMain() {
	const module = { handlers: {} };
	addMethodHandler(module, JUMP_AND_WAIT, jumpAndWait);
	addMethodHandler(module, RETURN_BACK, selfCallWrapper(returnBack));
	addMethodHandler(module, SAVE_RESULTS, selfCallWrapper(saveResults));
	addMethodHandler(module, SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
	executeHandler(module);
}
