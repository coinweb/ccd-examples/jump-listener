import {
	constructContractIssuer,
	type ContractIssuer,
} from "@coinweb/contract-kit";

export const START_WAITING = "START-WAITING";
export const PRODUCE_CLAIM = "PRODUCE-CLAIM";
export const PROCESS_CLAIM = "PROCESS-CLAIM";
export const TABLE_NAME = "CLAIMS";

export const jumperContractId = (): ContractIssuer => {
	return constructContractIssuer(
		"0x82e868a5925a09cf6b83ec69d96175af102166b592f048d8f18e87ec86681c83",
	);
};
