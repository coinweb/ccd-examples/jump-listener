import {
	type NewTx,
	getMethodArguments,
	constructContractIssuer,
	addMethodHandler,
	SELF_REGISTER_HANDLER_NAME,
	executeHandler,
	getContractId,
	selfCallWrapper,
	type Context,
	constructContractRef,
	type PreparedCallInfo,
	extractContractArgs,
	extractRead,
	extractBlock,
	type OrdJson,
	type Shard,
	type BlockFilter,
	constructContinueTx,
	type ClaimIssuer,
	constructRead,
	constructClaimKey,
	constructBlock,
	constructStore,
	constructClaim,
	toHex,
	bigIntReplacer,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";
import {
	START_WAITING,
	PRODUCE_CLAIM,
	jumperContractId,
	TABLE_NAME,
	PROCESS_CLAIM,
} from "../offchain/constants";

const filterForKey = (self: ClaimIssuer, key: OrdJson): BlockFilter => {
	return {
		issuer: self,
		first: TABLE_NAME,
		second: key,
	};
};

const prepareProduceJumperCall = (
	context: Context,
	shard: Shard,
	uniqueKey: OrdJson,
): PreparedCallInfo => {
	const self = constructContractIssuer(getContractId(context.tx));
	const filter = filterForKey(self, uniqueKey);
	const preparedCallInfo: PreparedCallInfo = {
		ref: constructContractRef(jumperContractId(), []),
		methodInfo: {
			methodName: "JUMP-AND-WAIT",
			methodArgs: [shard, filter],
		},
		contractInfo: {
			providedCweb: 4000n,
			authenticated: null,
		},
		contractArgs: [],
	};
	return preparedCallInfo;
};

const prepareProcessClaimCall = (
	context: Context,
	shard: Shard,
	uniqueKey: OrdJson,
): PreparedCallInfo => {
	const self = constructContractIssuer(getContractId(context.tx));
	const jumpFilter: BlockFilter = {
		issuer: jumperContractId(),
		first: self,
		second: [shard, filterForKey(self, uniqueKey)],
	};
	const preparedCallInfo: PreparedCallInfo = {
		ref: constructContractRef(self, []),
		methodInfo: {
			methodName: PROCESS_CLAIM,
			methodArgs: [],
		},
		contractInfo: {
			providedCweb: 1500n,
			authenticated: null,
		},
		contractArgs: [constructRead(jumpFilter.issuer, constructClaimKey(jumpFilter.first, jumpFilter.second)), constructBlock([jumpFilter])],
	};
	return preparedCallInfo;
};

const startWaiting = (context: Context): NewTx[] => {
	const [_, shard, uniqueKey] = getMethodArguments(context);
	const toJumperCall = prepareProduceJumperCall(context, shard, uniqueKey);
	const toProcessCall = prepareProcessClaimCall(context, shard, uniqueKey);
	return [
		constructContinueTx(context, [], [{ callInfo: toJumperCall }]),
		constructContinueTx(context, [], [{ callInfo: toProcessCall }]),
	];
};

const produceClaim = (context: Context): NewTx[] => {
	const [_, uniqueKey, body] = getMethodArguments(context);
	return [constructContinueTx(context, 
		[
			constructStore(constructClaim(constructClaimKey(TABLE_NAME, uniqueKey), body, toHex(0n)))
		]
	)];
};

const processClaim = (context: Context): NewTx[] => {
	const [readOp, blockOp] = extractContractArgs(context.tx);
	const results = extractRead(readOp)?.at(0);
	if (!results) {
		throw new Error("Invalid results");
	}
	const blockFilter = extractBlock(blockOp);
	if (!blockFilter || blockFilter.length === 0) {
		throw new Error("Invalid block filter");
	}
	const [filter, triggered] = blockFilter[0];
	if (!triggered) {
		throw new Error("Filter not triggered");
	}
	const uniqueKey = filter.second;
	return [constructContinueTx(context,
		[
			constructStore(constructClaim(constructClaimKey(TABLE_NAME, uniqueKey), results.content.body, toHex(0n)))
		]
	)];
};

export function cwebMain() {
	const module = { handlers: {} };
	addMethodHandler(module, START_WAITING, startWaiting);
	addMethodHandler(module, PRODUCE_CLAIM, produceClaim);
	addMethodHandler(module, PROCESS_CLAIM, selfCallWrapper(processClaim));
	addMethodHandler(module, SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
	executeHandler(module);
}
